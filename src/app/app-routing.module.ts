import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewtaskComponent } from './viewtask/viewtask.component';
import { AddupdatetaskComponent } from './addupdatetask/addupdatetask.component';

const routes: Routes = [
  { path: '', component: ViewtaskComponent }, 
  { path: 'ViewTaskManager', component: ViewtaskComponent },
  { path: 'Addupdatetask', component: AddupdatetaskComponent },
  { path: 'Addupdatetask/:id', component: AddupdatetaskComponent } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
