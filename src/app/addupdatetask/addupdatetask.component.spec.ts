import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AddupdatetaskComponent } from './addupdatetask.component';
import { BrowserModule} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms'
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from '../app.component';

describe('AddupdatetaskComponent', () => {
  let component: AddupdatetaskComponent;
  let fixture: ComponentFixture<AddupdatetaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, RouterTestingModule,        
        HttpClientModule,
        FormsModule,
        RouterModule        
      ],
      declarations: [ AddupdatetaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddupdatetaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
