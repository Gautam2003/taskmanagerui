import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskserviceService } from '../services/taskservice.service';
import { taskModel } from '../models/taskmodel';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-addupdatetask',
  templateUrl: './addupdatetask.component.html',
  styleUrls: ['./addupdatetask.component.css'],
  providers: [DatePipe]
})
export class AddupdatetaskComponent implements OnInit {

  taskID:string;
  taskDetail:taskModel;
  mode:string="Add";
  disabled:boolean=false;

  constructor(private taskService:TaskserviceService, private route:ActivatedRoute,private router:Router,private datePipe: DatePipe) { }

  ngOnInit() {   
    this.taskID = this.route.snapshot.paramMap.get('id');
    this.taskDetail=new taskModel();

    if(this.taskID)
    {
      let addTask= document.getElementById("updateAnchor");
      let viewTask= document.getElementById("viewAnchor");
      let submitButton= document.getElementById("submitBtn");
      let cancelButton= document.getElementById("clearBtn");
      addTask.innerHTML="Update Task";
      viewTask.innerHTML="";
      submitButton.innerHTML="Update";
      cancelButton.innerHTML="Cancel";
      this.mode="Edit";
      this.getTaskByID();
       
    }
  }

  getTaskByID()
  {    
    this.taskService.getAllTasksByID(this.taskID)
    .subscribe(result=>{  
      this.taskDetail=<taskModel>result;
      this.taskDetail.StartDate=this.datePipe.transform(this.taskDetail.StartDate, 'yyyy-MM-dd');
      this.taskDetail.EndDate=this.datePipe.transform(this.taskDetail.EndDate, 'yyyy-MM-dd');       
      if(this.taskDetail.Status=="Y")
      {
        this.disabled=true;
      }
      else{
        this.disabled=false;
      }
    })
  }

  onSubmit()
  {        
    if(this.mode=="Edit")
    {
      this.taskService.updateTask(this.taskDetail)
      .subscribe(result=>{
        this.router.navigateByUrl('/ViewTaskManager');
      })
    }
    else{
      this.taskService.addTask(this.taskDetail)
      .subscribe(result=>{
        this.router.navigateByUrl('/ViewTaskManager');
      })
       
    }
  }
  
  onClear()
  {     
    if(this.mode=="Edit")
    {
       this.router.navigateByUrl('/ViewTaskManager');
    }
    else{
      this.taskDetail=new taskModel();
    }
  }

  onDateSelect(value:any)
  {    
    var startdate=document.getElementById("startdate");
    this.taskDetail.StartDate=startdate.innerText;

  }

}
