import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { AppConstants } from '../constants';
import { taskModel } from '../models/taskmodel';

@Injectable({
  providedIn: 'root'
})
export class TaskserviceService {
  appconstants:AppConstants=new AppConstants();
  constructor(private httpClient:HttpClient) { }

  getAllTasks()
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.getTasks;
    return this.httpClient.get(url);
  }

  getAllTasksByID(taskID:string)
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.getTaskByID;
    let params=new HttpParams().set('id',taskID);
    return this.httpClient.get(url,{params:params});
  }

  addTask(task:taskModel)
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.addTask;     
    return this.httpClient.post(url,task);    
  }

  updateTask(task:taskModel)
  {
    let url:string=this.appconstants.baseUrl +this.appconstants.updateTask;     
    return this.httpClient.put(url,task);  
  }




}
