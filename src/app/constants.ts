export class AppConstants{

    constructor()
    {

    }

    //public baseUrl:string="http://localhost:51623/";
    public baseUrl:string="http://localhost/TaskManager.API/";    
    public getTasks:string="GetAllTasks";
    public getTaskByID:string="GetTaskByID";
    public addTask:string="PostTask";
    public updateTask:string="UpdateTask";    

}