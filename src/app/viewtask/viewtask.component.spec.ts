import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewtaskComponent } from './viewtask.component';
import { Component, OnInit } from '@angular/core';
import { taskModel } from '../models/taskmodel';
import { TaskserviceService } from '../services/taskservice.service';
import { DatePipe } from '@angular/common';
import { BrowserModule} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms'
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from '../app.component';
//import { spyOn } from 'jasmine';

describe('ViewtaskComponent', () => {
  let component: ViewtaskComponent;
  let fixture: ComponentFixture<ViewtaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, RouterTestingModule,        
        HttpClientModule,
        FormsModule,
        RouterModule        
      ],
      declarations: [ ViewtaskComponent,AppComponent ],
      providers: [TaskserviceService],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewtaskComponent);
    component = fixture.componentInstance;
    let taskserviceService:TaskserviceService;      
    let getValue:any={};
    getValue.innerHTML = 'Test';
    spyOn(component, 'setaddTask').and.returnValue(getValue);
    spyOn(component, 'setviewTask').and.returnValue(getValue);
    fixture.detectChanges();
  });

  it('should create', () => {    
    expect(component).toBeTruthy();
  });

  it('getAllTaskse', () => {    
    //expect(component).toBeTruthy();
    component.getAllTasks();
    
  });

  
});
