import { Component, OnInit } from '@angular/core';
import { taskModel } from '../models/taskmodel';
import { TaskserviceService } from '../services/taskservice.service';
import { DatePipe } from '@angular/common';



@Component({
  selector: 'app-viewtask',
  templateUrl: './viewtask.component.html',
  styleUrls: ['./viewtask.component.css'],
  providers: [DatePipe]
})
export class ViewtaskComponent implements OnInit {
   
  allTaskItems:taskModel[]=[];
  filterAllTaskItems:taskModel[]=[];
  taskItems:any;
  addTask:any;
  viewTask:any;


  constructor(private taskService:TaskserviceService,private datePipe: DatePipe ) { }

  ngOnInit() {          
     this.addTask= this.setaddTask();
     this.viewTask= this.setviewTask();
    this.addTask.innerHTML="Add Task";
    this.viewTask.innerHTML="View Task"; 
    this.getAllTasks();

  }

  setaddTask():any
  {
   return document.getElementById("updateAnchor");
  }
  setviewTask():any
  {
    return  document.getElementById("viewAnchor");
  }

  getAllTasks()
  {
    this.allTaskItems=[];
    this.filterAllTaskItems=[];
    this.taskService.getAllTasks()
    .subscribe(result=>{
      this.taskItems=result;
      this.allTaskItems=this.taskItems;
      this.allTaskItems.forEach(taskModelValue=>{
        let gettaskModel = new taskModel();
        gettaskModel.Priority = taskModelValue.Priority;
        gettaskModel.EndDate = taskModelValue.EndDate;
        gettaskModel.StartDate = taskModelValue.StartDate;
        gettaskModel.Task_ID = taskModelValue.Task_ID;
        gettaskModel.Parent_ID = taskModelValue.Parent_ID;
        gettaskModel.TaskName = taskModelValue.TaskName;
        gettaskModel.Mode = taskModelValue.Mode;
        if(taskModelValue.Status=="Y")
        {
          gettaskModel.DisabledMode=true;
        }
        else
        {
          gettaskModel.DisabledMode=false;
        }
        
        this.filterAllTaskItems.push(gettaskModel);


      })
    })
  }

  EndTask(value:any)
  {   
    value.Status="Y";    
    this.taskService.updateTask(value)
    .subscribe(result=>{
      this.getAllTasks();
    })
  }

  search(task:any,parenttask:any,PriorityFrom:any,Priorityto:any,StartDate:any,EndDate:any):void
  {    
    let filteredTaskItems:taskModel[]=this.allTaskItems;
   
    if(task.value)
    {
      filteredTaskItems=filteredTaskItems.filter(A=>A.TaskName.toLocaleUpperCase().includes(task.value.toString().toLocaleUpperCase())) ;
    }            
    if(parenttask.value)
    {
      filteredTaskItems=filteredTaskItems.filter(A=>A.Parent_ID==parenttask.value);
    }
    if(Priorityto.value)
    {
      filteredTaskItems=filteredTaskItems.filter(A=>parseInt(A.Priority)<=parseInt(Priorityto.value)) 
    }
    if(PriorityFrom.value)
    {
      filteredTaskItems=filteredTaskItems.filter(A=>parseInt(A.Priority)>=parseInt(PriorityFrom.value)) 
    }
    
    if(StartDate.value)
    {          
      

      filteredTaskItems=filteredTaskItems.filter(A=> Date.parse(A.StartDate)>=Date.parse(StartDate.value)) 
    }
    
    if(EndDate.value)
    {      
      filteredTaskItems=filteredTaskItems.filter(A=> Date.parse(A.EndDate)<=Date.parse(EndDate.value)) 
    }    
    
    this.filterAllTaskItems=filteredTaskItems;
  }

  getdateValue(value:any):string{

    let pipedValue=this.datePipe.transform(value, 'dd/MM/yyyy');
     return pipedValue;
  }

}
