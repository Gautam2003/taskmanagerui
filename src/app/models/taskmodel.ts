export class taskModel{

    public Task_ID:string;
    public Parent_ID:string;
    public TaskName:string;
    public Priority:string;
    public StartDate:string;
    public EndDate:string;
    public Mode:string;
    public Status:string;
    public DisabledMode:boolean=false;

    constructor()
    {
        this.Priority="0";
    }
}

